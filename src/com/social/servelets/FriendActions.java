package com.social.servelets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.social.database.UserDbUtil;
import com.social.model.User;

/**
 * Servlet implementation class FriendActions
 */
@WebServlet("/FriendActions")
public class FriendActions extends HttpServlet
{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FriendActions() 
    {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    @Resource(name="jdbc/project")
    private DataSource datasource;
    private UserDbUtil userdb;
    
    @Override
   	public void init(ServletConfig config) throws ServletException 
       {
   	// TODO Auto-generated method stub
   	super.init(config);
   	
   	try
   	{
   		userdb = new UserDbUtil(datasource);
   	}
   	catch(Exception ex)
   	{
   		throw new ServletException(ex);
   	}
   	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession  session = request.getSession();
		
		User temp = (User) session.getAttribute("user");
		
		String send = request.getParameter("add");
		String accept = request.getParameter("accept");
		
		String decline = request.getParameter("decline");
		String remove = request.getParameter("remove");
		
		if(send != null) 
		{
			temp.sendRequest(send, userdb);			
		}
		
		if(accept != null)
		{
			temp.accept(accept, userdb);
		}
		
		if(decline != null)
		{
			temp.decline(decline, userdb);
		}
		
		
		if(remove != null)
		{
			temp.removeFriend(remove, userdb);
		}
		
		response.sendRedirect("Profile");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
