package com.social.servelets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.social.database.UserDbUtil;
import com.social.model.User;

/**
 * Servlet implementation class Post
 */
@WebServlet("/Post")
public class Post extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post() 
    {
        super();
        // TODO Auto-generated constructor stub
    }
    @Resource(name="jdbc/project")
    private DataSource datasource;
    private UserDbUtil userdb;
    
    @Override
   	public void init(ServletConfig config) throws ServletException 
       {
   	// TODO Auto-generated method stub
   	super.init(config);
   	
   	try
   	{
   		userdb = new UserDbUtil(datasource);
   	}
   	catch(Exception ex)
   	{
   		throw new ServletException(ex);
   	}
   	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession  session = request.getSession();
		
		User temp = (User) session.getAttribute("user");
		
		String page = request.getParameter("page");

		String delete = request.getParameter("delete");
		String content = request.getParameter("content");
//		
//		if(temp != null && !request.getParameter("post").isEmpty())
//		{
//			if(request.getParameter("edit").equals("edit"))
//			{
//			
//			}
		
			if(delete != null)
			{
				temp.deletePost(delete,userdb);
			}
		
		
			// create post
			if(content != null)
			{
				 
				 System.out.println(content);
				 
				temp.createPost(content,userdb);
			}
			 	
			 
			 
			 response.sendRedirect(page);


		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
