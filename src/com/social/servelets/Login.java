package com.social.servelets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.social.database.UserDbUtil;
import com.social.model.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login()
    {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Resource(name="jdbc/project")
    private DataSource datasource;
    private UserDbUtil userdb;
 
   
    @Override
	public void init(ServletConfig config) throws ServletException 
    {
	// TODO Auto-generated method stub
	super.init(config);
	
	try
	{
		userdb = new UserDbUtil(datasource);
	}
	catch(Exception ex)
	{
		throw new ServletException(ex);
	}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		String email = request.getParameter("emailid");
		String password = request.getParameter("password");
		
		User tempUser = new User(email, password);
		
		boolean success = tempUser.login(userdb);
		if(success)
		{
			HttpSession  session = request.getSession();
			session.setAttribute("user",tempUser);
			response.sendRedirect("Home.jsp");
			
		}
		else
		{
			request.setAttribute("Loginerror", true);
			RequestDispatcher dispatcher = request.getRequestDispatcher("Login");
			response.sendRedirect("Login.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
