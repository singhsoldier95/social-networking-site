package com.social.servelets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.social.database.UserDbUtil;
import com.social.model.User;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registration() {
        super();
        
        
        // TODO Auto-generated constructor stub
        
    }
    @Resource(name="jdbc/project")
    private DataSource datasource;
    private UserDbUtil userdb;
 
   
    @Override
	public void init(ServletConfig config) throws ServletException 
    {
	// TODO Auto-generated method stub
	super.init(config);
	
	try
	{
		userdb = new UserDbUtil(datasource);
	}
	catch(Exception ex)
	{
		throw new ServletException(ex);
	}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String firstName = request.getParameter("first_name");
		String lastName  = request.getParameter("last_name");
		String email = request.getParameter("emailid");
		String password = request.getParameter("password");
		
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(email);
		System.out.println(password);

		User userData = new User(email, password, firstName, lastName);
		
		
		boolean created = userData.register(userdb);
		
			if(created)
			{
				HttpSession session = request.getSession();
				session.setAttribute("email", email);
				response.sendRedirect("Index.jsp");
			}
			else
			{
				RequestDispatcher dispatcher = request.getRequestDispatcher("Register.jsp");
				request.setAttribute("registerError", true);
				dispatcher.forward(request, response);
			}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
