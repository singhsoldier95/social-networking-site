package com.social.servelets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.social.database.UserDbUtil;
import com.social.model.User;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Resource(name="jdbc/project")
    private DataSource datasource;
    private UserDbUtil userdb;
    
    @Override
   	public void init(ServletConfig config) throws ServletException 
       {
   	// TODO Auto-generated method stub
   	super.init(config);
   	
   	try
   	{
   		userdb = new UserDbUtil(datasource);
   	}
   	catch(Exception ex)
   	{
   		throw new ServletException(ex);
   	}
   	}
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		HttpSession  session = request.getSession();
		
		User temp = (User) session.getAttribute("user");
		
		if(temp != null)
		{
			userdb.myPosts(temp);
			userdb.savedPosts(temp);
			
			userdb.getRequest(temp);
			
			userdb.getFriends(temp);
			
			session.setAttribute("allusers",userdb.getAllUsers());
			
		}
		
		
		session.setAttribute("user", temp);
		
		response.sendRedirect("Profile.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
