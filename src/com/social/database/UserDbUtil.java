package com.social.database;
import java.sql.Timestamp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.sql.DataSource;

import com.social.model.Posts;
import com.social.model.User;
import java.util.Date;

public class UserDbUtil 
{
	private Timestamp postDate;

	private DataSource datasource;
	
	public UserDbUtil(DataSource datasource)
	{
		this.datasource =  datasource;
	}
	
	//new registration
	public void Insert(User user) throws SQLException 
	{
		System.out.println("insert");
		System.out.println(user);
		
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String email = user.getEmail();
		String password = user.getPassword();
		
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		try 
		{
			conn = this.datasource.getConnection();
			
			String sql = String.format("INSERT INTO user VALUES('%s','%s','%s','%s','%s')", firstName, lastName, email, password, "");
			
			stm = conn.createStatement(); 
			
			stm.executeUpdate(sql);	
			
						
		}
		finally 
		{
			close(conn, stm,res);
		}
		
		
	}
	
	
	//login validation
	
	public User checkProfile(User user) throws SQLException
	{
		String email = user.getEmail();
		String password = user.getPassword();
		
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		try 
		{
			conn = this.datasource.getConnection();
			
			String sql = " SELECT * FROM user WHERE email  = ?";
			PreparedStatement  ps = conn.prepareStatement(sql);
			ps.setString(1, user.getEmail());
			res = ps.executeQuery();
			
			res.next();
			
			String firstName =  res.getString("FirstName");
			String lastName = res.getString("LastName");
			String emailId = res.getString("Email");
			String pass = res.getString("Password");
			
			
			User userdetails = new  User(emailId, pass, firstName, lastName);
			
			return userdetails;
			
						
		} 
		finally 
		{
			close(conn, stm,res);
		}
	}	
	
		//getting the all the posts
	public User allPosts(User temp )
	{
			Connection conn = null;
			Statement stm = null;
			ResultSet res = null;
			ArrayList<Posts> posts= new ArrayList<>();
			
			try 
			{
				conn = this.datasource.getConnection();
				String sql = "SELECT * FROM  posts";
				stm = conn.createStatement();
				res = stm.executeQuery(sql);
				
				while(res.next())
				{
					String postid =  res.getString("postID");
					String postowner = res.getString("email");
					String postcontent = res.getString("content");
					String image = res.getString("image");
					Timestamp postdate = res.getTimestamp("date");
					
					posts.add(new Posts(postid, postowner, postcontent, image, postdate));
				}
				System.out.println(posts);
			temp.setPosts(posts);
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return temp;
			
		}
	
	//user's posts
	public User myPosts(User temp)
	{
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		ArrayList<Posts> posts= new ArrayList<>();
		
		try 
		{
			conn = this.datasource.getConnection();
			String sql = "SELECT * FROM  posts where  email = ?";
			PreparedStatement  ps = conn.prepareStatement(sql);
			ps.setString(1, temp.getEmail());
			res = ps.executeQuery();
			
			while(res.next())
			{
				String postid =  res.getString("postID");
				String postowner = res.getString("email");
				String postcontent = res.getString("content");
				String image = res.getString("image");
				Timestamp postdate = res.getTimestamp("date");
				
				posts.add(new Posts(postid, postowner, postcontent, image, postdate));
			}
			System.out.println(posts);
			temp.setPosts(posts);
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return temp;
	}
	
	
	//saved posts
	public User savedPosts(User temp)
	{
		return temp;
	}
	
	
	//create post
	public void createPost(Posts post) throws SQLException
	{
		String postId = post.getPostId();
		String postContent = post.getPostContent();
		String postOwner = post.getPostOwner();
		
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		this.postDate = new Timestamp(new Date().getTime());

		
		try 
		{
			conn = this.datasource.getConnection();
			
			String sql = String.format("INSERT INTO posts VALUES('%s','%s','%s','%s','%s')", postId, postOwner, postContent, "", this.postDate);
			
			stm = conn.createStatement(); 
			
			int r = stm.executeUpdate(sql);	

			System.out.println(r);
		}
		finally 
		{
			close(conn, stm,res);
		}
		
	}
	
	//delete post
	public void deletePost(String deleteid) throws SQLException
	{
		
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		System.out.println(deleteid);
	
		try 
		{
			conn = this.datasource.getConnection();
			
			String sql = "DELETE FROM posts WHERE postID=?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, deleteid);
			int r = ps.executeUpdate();	
			System.out.println(r);
		}
		finally 
		{
			close(conn, stm,res);
		}
			
	}
	
	//edit post
	public Posts editPost(Posts temp)
	{
		return temp;
	}
	
	//getting all the users
	public ArrayList<User> getAllUsers()
	{
		ArrayList<User> users = new ArrayList<>();
		
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		try 
		{
			conn = this.datasource.getConnection();
			String sql = "SELECT * FROM  user";
			stm = conn.createStatement();
	
			res = stm.executeQuery(sql);
			
			while(res.next())
			{
				String FirstName = res.getString("FirstName");
				String LastName = res.getString("LastName");
				String Email = res.getString("Email");
				
				users.add(new User(Email, null, FirstName, LastName));
			}
			
			
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return users;
	}
	
	//adding friend
	public void sendRequest(String email,String femail)
	{
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		try 
		{
			conn = this.datasource.getConnection();
			String sql = "INSERT INTO friends VALUES(?,?,?)";
			PreparedStatement pr = conn.prepareStatement(sql);
			
			
			pr.setString(1,email );
			pr.setString(2,femail );
			pr.setString(3, "0");
			pr.executeUpdate();
			
	
			
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	//accept friend
		public void accept(String email,String femail)
		{
			Connection conn = null;
			Statement stm = null;
			ResultSet res = null;
			
			try 
			{
				conn = this.datasource.getConnection();
				String sql = "Update friends set status=? where RelatedUserEmail=? and RelatingUserEmail=?";
				PreparedStatement pr = conn.prepareStatement(sql);
				String sql2 = "Insert into friends values(?,?,?)";
				PreparedStatement pr2 = conn.prepareStatement(sql2);
				
				
				pr.setString(1, "1");
				pr.setString(2,email );
				pr.setString(3,femail );
				pr.executeUpdate();
				
				
				pr2.setString(1,femail );
				pr2.setString(2,email );
				pr2.setString(3, "1");
				pr2.executeUpdate();
				
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		//decline friend
		public void decline(String email,String femail)
		{
			Connection conn = null;
			Statement stm = null;
			ResultSet res = null;
			
			try 
			{
				conn = this.datasource.getConnection();
				String sql = "Delete from friends where RelatedUserEmail=? and RelatingUserEmail=? and status=?";
				PreparedStatement pr = conn.prepareStatement(sql);
				
				
				
				pr.setString(1,email );
				pr.setString(2,femail );
				pr.setString(3, "0");
				pr.executeUpdate();
	
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		
		
		//remove friend
				public void removeFriend(String email,String femail)
				{
					Connection conn = null;
					Statement stm = null;
					ResultSet res = null;
					
					try 
					{
						conn = this.datasource.getConnection();
						String sql = "DELETE from friends where RelatedUserEmail=? and RelatingUserEmail=? and status=?";
						PreparedStatement pr = conn.prepareStatement(sql);
						String sql2 = "DELETE from friends where RelatedUserEmail=? and RelatingUserEmail=? and status=?";
						PreparedStatement pr2 = conn.prepareStatement(sql2);
						
						
						pr.setString(1,email );
						pr.setString(2,femail );
						pr.setString(3, "1");
						pr.executeUpdate();
						
						
						pr2.setString(1,femail );
						pr2.setString(2,email );
						pr2.setString(3, "1");
						pr2.executeUpdate();
						
					}
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
	
	//get requests
	public void getRequest(User temp)
	{
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		ArrayList<User> tempList = new ArrayList<User>();
		
		try 
		{
			conn = this.datasource.getConnection();
			String sql = "Select * from user WHERE email IN (SELECT RelatingUserEmail from friends WHERE RelatedUserEmail=? AND status=?)";
			PreparedStatement pr = conn.prepareStatement(sql);
			
			
			pr.setString(1,temp.getEmail());
			pr.setString(2,"0");
		
			res = pr.executeQuery();
			
			
			while(res.next())
			{
				String FirstName = res.getString("FirstName");
				String LastName = res.getString("LastName");
				String Email = res.getString("Email");
				
				 tempList.add(new User(Email, null, FirstName, LastName));
			}
			
			temp.setRequests(tempList);
			
	
			
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	//friends
	public void getFriends(User temp)
	{
		Connection conn = null;
		Statement stm = null;
		ResultSet res = null;
		
		ArrayList<User> tempList = new ArrayList<User>();
		
		try 
		{
			conn = this.datasource.getConnection();
			String sql = "Select * from user WHERE email IN (SELECT RelatingUserEmail from friends WHERE (RelatedUserEmail=? OR RelatingUserEmail=?)  AND status=?)";
			PreparedStatement pr = conn.prepareStatement(sql);
			
			
			pr.setString(1,temp.getEmail());
			pr.setString(2,temp.getEmail());
			pr.setString(3,"1");
		
			res = pr.executeQuery();
			
			
			while(res.next())
			{
				String FirstName = res.getString("FirstName");
				String LastName = res.getString("LastName");
				String Email = res.getString("Email");
				
				 tempList.add(new User(Email, null, FirstName, LastName));
			}
			
			temp.setFriends(tempList);
			
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	//remove friend
		public void removeFriend(String email)
		{
			
		}
	
	//closing connection
		private void close(Connection conn, Statement stm, ResultSet res) 
		{
			try 
			{
				if(res != null) 
				{
					res.close();
				}
				if(stm != null) 
				{
					stm.close();
				}
				if(conn != null) 
				{
					conn.close();
				}
			}
			
			catch(Exception exe) 
			{
			 	exe.printStackTrace();
			}
		}
		
}

		
	
	
	