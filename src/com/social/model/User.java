package com.social.model;

import java.sql.SQLException;
import java.util.ArrayList;

import com.social.database.UserDbUtil;

public class User
{
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private ArrayList<Posts> posts = null;
	private ArrayList<User> friends = null;
	private ArrayList<User> requests = null;
	
	
	public ArrayList<User> getFriends() {
		return this.friends;
	}

	public void setFriends(ArrayList<User> friends) {
		this.friends = friends;
	}

	public ArrayList<User> getRequests() {
		return this.requests;
	}

	public void setRequests(ArrayList<User> requests) {
		this.requests = requests;
	}

	public User(String email, String password, String firstName, String lastName) 
	{
		
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public User(String email, String password) 
	{
		
		this.email = email;
		this.password = password;
	}
	



	public String getEmail() {
		return this.email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getPassword() {
		return this.password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public ArrayList<Posts> getPosts()
	{
		return posts;
	}

	public void setPosts(ArrayList<Posts> posts) 
	{
		this.posts = posts;
	}

	
	
	//registering a new user
	public boolean register(UserDbUtil userdb )
	{
		try 
		{
			userdb.Insert(this);
			return true;
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	
	//checking profile details in the database
	public boolean login(UserDbUtil userdb)
	{
		try 
		{
			User dbuser = userdb.checkProfile(this);
			if(dbuser != null)
			{
				if(this.password.equals(dbuser.getPassword())) 
				{
					this.firstName = dbuser.firstName;
					this.lastName = dbuser.lastName;
					this.email = dbuser.email;
					this.password = null;
					return true;

				}
			}
			
		}
		catch (SQLException e) 
		{
			
			e.printStackTrace();
		}
		return false;
	}
	
	//create post
	public void createPost(String content, UserDbUtil userdb)
	{
		try {
			userdb.createPost(new Posts(this.email, content));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//delete post
	public void deletePost(String delete, UserDbUtil userdb)
	{
		try 
		{
			userdb.deletePost(delete);
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//adding friend
	public void sendRequest(String email, UserDbUtil userdb)
	{
		userdb.sendRequest(this.email,email);
	}
	
	//accepting 
	public void accept(String email, UserDbUtil userdb)
	{
		userdb.accept(this.email,email);
	}
	
	public void decline(String email, UserDbUtil userdb)
	{
		userdb.decline(this.email,email);
	}
	
	
	
	
	public void removeFriend(String email, UserDbUtil userdb)
	{
		userdb.removeFriend(this.email,email);
	}
	

}
