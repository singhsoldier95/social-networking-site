package com.social.model;

import java.util.Date;

public class Messages 
{
	private String messageId;
	private String messageContent;
	private String senderMessageId;
	private String receiverMessageId;
	
	
	public Messages(String messageId, String messageContent, String senderMessageId, String receiverMessageId,
			Date messageDate) 
	{
		this.messageId = messageId;
		this.messageContent = messageContent;
		this.senderMessageId = senderMessageId;
		this.receiverMessageId = receiverMessageId;
		this.messageDate = messageDate;
	}
	
	public String getSenderMessageId() 
	{
		return senderMessageId;
	}
	public void setSenderMessageId(String senderMessageId) 
	{
		this.senderMessageId = senderMessageId;
	}
	public String getReceiverMessageId() 
	{
		return receiverMessageId;
	}
	public void setReceiverMessageId(String receiverMessageId) 
	{
		this.receiverMessageId = receiverMessageId;
	}
	private Date messageDate;
	
	public String getMessageId() 
	{
		return messageId;
	}
	
	public void setMessageId(String messageId) 
	{
		this.messageId = messageId;
	}
	public String getMessageContent() 
	{
		return messageContent;
	}
	public void setMessageContent(String messageContent)
	{
		this.messageContent = messageContent;
	}
	public Date getMessageDate() 
	{
		return messageDate;
	}
	public void setMessageDate(Date messageDate) 
	{
		this.messageDate = messageDate;
	}
	
	
}
