package com.social.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Posts 
{
	private String postId;
	private String postContent;
	private ArrayList<String> likes;
	private ArrayList<String> comments;
	private String email;
	private String image;
	private Timestamp postDate;
	private String postOwner;

	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPostOwner() {
		return postOwner;
	}

	public void setPostOwner(String postOwner) {
		this.postOwner = postOwner;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email) 
	{
		this.email = email;
	}

	public Date getPostDate()
	{
		return postDate;
	}

	public void setPostDate(Timestamp postDate)
	{
		this.postDate = postDate;
	}
	
	//for creating a post
	public Posts(String postowner, String postcontent)
	{
		this.postId = Long.toString(new Date().getTime());
		this.postOwner = postowner;
		this.postContent = postcontent;
		this.postDate = new Timestamp(new Date().getTime());
	}
	
	
	
	//for adding a post
	public Posts(String postId, String postowner, String postContent, String image, Timestamp postDate)
	{
		
		this.postId = postId;
		this.postOwner = postowner;
		this.postContent = postContent;
		this.image = image;
		this.likes = likes;
		this.comments = comments;
		this.postDate = postDate;
	}
	
	public String getPostId() 
	{
		return this.postId;
	}
	public void setPostId(String postId) 
	{
		this.postId = postId;
	}
	public String getPostContent() 
	{
		return postContent;
	}
	public void setPostContent(String postContent)
	{
		this.postContent = postContent;
	}
	public ArrayList<String> getLikes()
	{
		return likes;
	}
	public void setLikes(ArrayList<String> likes) 
	{
		this.likes = likes;
	}
	public ArrayList<String> getComments()
	{
		return comments;
	}
	public void setComments(ArrayList<String> comments)
	{
		this.comments = comments;
	}
	public Date getDate()
	{
		return postDate;
	}
	public void setDate(Timestamp postDate)
	{
		this.postDate = postDate;
	}
	
	
}
