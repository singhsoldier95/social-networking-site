<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="tag" %>
    <%@ page import="com.social.model.*"  %>
    
    
    
	<% 
		
		User temp = (User) session.getAttribute("user");
		
		if(temp == null)
		{
			response.sendRedirect("Index.jsp");
		}
		
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
	<form action="Post">
		<a href = "Profile" alt = "myProfile">My profile</a>
		<a href = "Logout" alt = "index">Log out</a>
	  	<input type="text" placeholder="Search..">
	  	<br><br>
	  	<textarea name="content" rows="10" cols="60" placeholder = "What's on your mind?"></textarea>
  		<br><br>
  		<button type="submit" value="Home" name="page">Post</button>
	</form>
	<div>
	
	<tag:forEach var="post" items="${user.getPosts()}">
	
	
	
	<div>
		<h1>${post.getPostContent()}</h1>
		
		<form action="Post">
			<button name="edit" value = "edit" >Edit post</button>
		</form>
		
		<form action = "Post">
			<input type="hidden" name="page" value="Home"/>
			<button name="delete" value = "${post.getPostId()}">Delete post</button>
		</form>
		
	</div>
		
		
	
	</tag:forEach>
	
	
	
	</div>
</body>
</html>