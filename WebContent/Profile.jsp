<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="tag" %>
<%@ page import="com.social.model.*"  %>
<%@ page import="java.util.*"  %>

<% 
		
		User temp = (User) session.getAttribute("user");

		ArrayList<User> allusers = (ArrayList<User>) session.getAttribute("allusers");
		
		
		
		
		if(temp == null)
		{
			response.sendRedirect("Index.jsp");
		}
		
		
	%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My profile</title>
</head>
<body>


	



	<form action="Post">
		<a href = "Home" alt = "home">News feed</a>
		<a href = "Logout" alt = "index">Log out</a>
	  	<input type="text" placeholder="Search..">
	  	<br><br>
	  	<textarea name="post" rows="10" cols="60" placeholder = "What's on your mind?"></textarea>
  		<br><br>
  
  		<button type="submit" value="Profile" name="page">Post</button>
	</form>
	

	<div id="users" style="display:none;">
		<h1>All User</h1>
		<tag:forEach var ="user" items="${allusers}" >
			<div class="user">
				<span> ${user.getFirstName() }</span>
				<form action="FriendActions">
					<button name="add" value="${user.getEmail()}">add friend</button>
				</form>
				
			</div>
		</tag:forEach>
	
		
	</div>

	<div id="request" >
		<h3>All Request</h3>
		<%
			for(User user:temp.getRequests())
			{
				out.println("<h5>" + user.getFirstName() + "</h5>");
				out.println("<form action='FriendActions'><button name='accept' value= " + user.getEmail() + " > Accept</button></form>");
				out.println("<form action='FriendActions'><button name='decline' value= " + user.getEmail() + " >Decline</button></form>");
			}
			
		
		
		%>
	</div>
	
	<div id="friends">
		<h3>All Friends</h3>
		
		<%
			for(User user:temp.getFriends())
			{
				out.println("<h5>" + user.getFirstName() + "</h5>");
				out.println("<form action='FriendActions'><button name='remove' value= " + user.getEmail() + " >Remove</button></form>");
			}
		%>
	
		
	</div>
	
	<tag:forEach var="post" items="${user.getPosts()}">
	
	<div>
		<h1>${post.getPostContent()}</h1>
		<button name="edit" >Edit post</button>
		<button name="delete" value="${post.getPostId() }">Delete post</button>
	</div>
	
	</tag:forEach>

</body>
</html>